-- Your SQL goes here

CREATE TABLE item (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  quantity INTEGER NOT NULL
)