#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel_migrations;


use crate::rocket::serde::ser::SerializeSeq;
use archi_and_infra_back::{
    models::Items,
    schema::item::{name, quantity},
};
use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel_migrations::EmbeddedMigrations;
use crate::diesel_migrations::MigrationHarness;
use dotenvy::dotenv;
use rocket::{
    fairing::{Fairing, Info, Kind},
    http::{ContentType, Header, Method, Status},
    response::status::BadRequest,
    serde::{json::Json, Deserialize, Serialize, Serializer},
    Request, Response,
};
use std::env;
pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("./migrations");

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
struct ItemEntity {
    name: String,
    quantity: i32,
}

#[derive(Serialize)]
#[serde(crate = "rocket::serde")]
struct MyResponse {
    status: String,
}

impl std::fmt::Debug for ItemEntity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Item {{ name: {}, quantity: {} }}",
            self.name, self.quantity
        )
    }
}

#[post("/item", data = "<item>")]
fn create_item(item: Json<ItemEntity>) -> Result<Json<MyResponse>, BadRequest<String>> {
    use archi_and_infra_back::schema::item;

    // check for valid values
    if item.name.is_empty() || item.quantity <= 0 {
        return Err(BadRequest(Some("invalid body".to_string())));
    }
    let item = item.into_inner();

    let pg_con = &mut establish_connection();

    let item = Items {
        name: item.name,
        quantity: item.quantity,
    };

    diesel::insert_into(item::table)
        .values(&item)
        .execute(pg_con)
        .expect("Error inserting item");

    println!(
        "Item {{ name: {}, quantity: {} }}",
        item.name, item.quantity
    );

    let response = MyResponse {
        status: "ok".to_string(),
    };
    return Ok(Json(response));
}

struct ItemEntityList {
    item_entity_list: Vec<ItemEntity>,
}

impl Serialize for ItemEntityList {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.item_entity_list.len()))?;
        for e in &self.item_entity_list {
            seq.serialize_element(&e)?;
        }
        seq.end()
    }
}

#[get("/")]
fn index() -> Json<ItemEntityList> {
    use archi_and_infra_back::schema::item;

    let pg_con = &mut establish_connection();

    let all_items: Vec<Items> = item::table
        .select((name, quantity))
        .load(pg_con)
        .expect("Error loading items");

    let all_entity: Vec<ItemEntity> = all_items
        .into_iter()
        .map(|item| ItemEntity {
            name: item.name,
            quantity: item.quantity,
        })
        .collect();

    return Json(ItemEntityList {
        item_entity_list: all_entity,
    });
}

pub fn establish_connection() -> PgConnection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let mut db_conn = PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url));
    
    // wrap it in match to avoid panic
    match db_conn.run_pending_migrations(MIGRATIONS) {
        Ok(_) => println!("Migrations ran successfully"),
        Err(e) => println!("Error running migrations: {}", e),
    }
    
    return db_conn;
}

pub struct CORS;

#[rocket::async_trait]
impl Fairing for CORS {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to responses",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, GET, PATCH, OPTIONS",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Headers", "Content-Type"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));

        if _request.method() == Method::Options {
            response.set_status(Status::NoContent);
            response.set_header(Header::new(
                "Access-Control-Allow-Methods",
                "POST, PATCH, GET, DELETE",
            ));
            response.set_header(Header::new(
                "Access-Control-Allow-Headers",
                "content-type, authorization",
            ));
        }
    }
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(CORS)
        .mount("/", routes![index, create_item])
}
