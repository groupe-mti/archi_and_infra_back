use diesel::prelude::*;
use crate::schema::item;

#[derive(Queryable, Insertable)]
#[diesel(table_name = item)]
pub struct Items{
    pub name: String,
    pub quantity: i32,
}