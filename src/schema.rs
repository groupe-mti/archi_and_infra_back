// @generated automatically by Diesel CLI.

diesel::table! {
    item (id) {
        id -> Int4,
        name -> Varchar,
        quantity -> Int4,
    }
}
